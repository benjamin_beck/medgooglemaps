<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

$GLOBALS['TCA']['tx_medgooglemaps_domain_model_marker'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_medgooglemaps_domain_model_marker']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, address, zip, city, country, geocode, latitude, longitude, infoText, infowindowautoopen, navigation, markercolor, markercustom'
	),
	'types' => array(
		'1' => array(
			'showitem' => 'l10n_parent, l10n_diffsource,			
			--palette--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.paletteAddress;paletteAddress,
			
			geocode;;paletteLatLng,

			--div--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.tabInfowindow,
			 	infoText, infowindowautoopen, navigation,

			--div--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.tabOptions,
				--palette--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.paletteCore;paletteCore,
				--palette--;LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.paletteMarker;paletteMarker,	
			'
		)
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'hidden, sys_language_uid',
			'canNotCollapse' => TRUE
		),
		'paletteAddress' => array(
			'showitem' => 'address, --linebreak--, zip, city, --linebreak--, country',
			'canNotCollapse' => TRUE
		),
		'paletteLatLng' => array(
			'showitem' => 'latitude, longitude',
			'canNotCollapse' => TRUE
		),
		'paletteMarker' => array(
			'showitem' => 'markercolor, --linebreak--, markercustom',
			'canNotCollapse' => TRUE
		)
	),
	'columns' => array(
		
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array(
						'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
						-1
					),
					array(
						'LLL:EXT:lang/locallang_general.xlf:LGL.default_value',
						0
					)
				)
			)
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array(
						'',
						0
					)
				),
				'foreign_table' => 'tx_medgooglemaps_domain_model_marker',
				'foreign_table_where' => 'AND tx_medgooglemaps_domain_model_marker.pid=###CURRENT_PID### AND tx_medgooglemaps_domain_model_marker.sys_language_uid IN (-1,0)'
			)
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough'
			)
		),
		
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255
			)
		),
		
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check'
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				)
			)
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				)
			)
		),
		
		'address' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.address',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			)
		),
		
		'zip' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.zip',
			'config' => array(
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim, required'
			)
		),
		
		'city' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.city',
			'config' => array(
				'type' => 'input',
				'size' => 19,
				'eval' => 'trim, required'
			)
		),
		
		'country' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.country',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			)
		),
		
		'geocode' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.geocode',
			'config' => array(
				'form_type' => 'user',
				'userFunc' => 'MED\Medgooglemaps\Utility\Geocode->geocodeSectionIrre'
			)
		),
		
		'latitude' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.latitude',
			'config' => array(
				'type' => 'input',
				'size' => 8,
				'eval' => 'trim'
			)
		),
		
		'longitude' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.longitude',
			'config' => array(
				'type' => 'input',
				'size' => 8,
				'eval' => 'trim'
			)
		),
		
		'infoText' => array(
			'exclude' => 1,
			'l10n_mode' => 'prefixLangTitle',
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.infoText',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 5,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords' => 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
			'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]'
		),
		
		'infowindowautoopen' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.infowindowautoopen',
			'config' => array(
				'type' => 'check'
			)
		),
		
		'navigation' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.navigation',
			'config' => array(
				'type' => 'check'
			)
		),
		
		'markercolor' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array(
						'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.1',
						'marker-red'
					),
					array(
						'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.2',
						'marker-green'
					),
					array(
						'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.3',
						'marker-blue'
					),
					array(
						'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.4',
						'marker-yellow'
					),
					array(
						'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.5',
						'marker-orange'
					),
					array(
						'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.6',
						'marker-black'
					),
					array(
						'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercolor.7',
						'marker-white'
					)
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			)
		),
		
		'markercustom' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_db.xlf:tx_medgooglemaps_domain_model_marker.markercustom',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'uploadfolder' => 'uploads/pics',
				'show_thumbs' => 1,
				'size' => 1,
				'maxitems' => '1',
				'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
				'disallowed' => '',
				'max_size' => 20000
			)
		)
		
	)
);