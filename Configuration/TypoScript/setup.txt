plugin.tx_medgooglemaps {
  view {
    templateRootPath = {$plugin.tx_medgooglemaps.view.templateRootPath}
    partialRootPath = {$plugin.tx_medgooglemaps.view.partialRootPath}
    layoutRootPath = {$plugin.tx_medgooglemaps.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_medgooglemaps.persistence.storagePid}
  }
  features {
    # uncomment the following line to enable the new Property Mapper.
    # rewrittenPropertyMapper = 1
  }
  settings {
    apiUrl = {$plugin.tx_medgooglemaps.settings.apiUrl}
    jsFile = {$plugin.tx_medgooglemaps.settings.jsFile}
    cssFile = {$plugin.tx_medgooglemaps.settings.cssFile}
    addJquery = {$plugin.tx_medgooglemaps.settings.addJquery}
    addToFooter = {$plugin.tx_medgooglemaps.settings.addToFooter}
    googleMapsSubmitClasses = {$plugin.tx_medgooglemaps.settings.googleMapsSubmitClasses}		
  }
}
