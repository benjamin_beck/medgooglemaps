<?php
namespace MED\Medgooglemaps\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use RZ\Medgooglemaps\Utility\T3jquery;

/**
 * MapsController
 */
class MapsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * mapsRepository
	 *
	 * @var \MED\Medgooglemaps\Domain\Repository\MapsRepository
	 * @inject
	 */
	protected $mapsRepository = NULL;
	
	/**
	 * markerRepository
	 *
	 * @var \MED\Medgooglemaps\Domain\Repository\MarkerRepository
	 * @inject
	 */
	protected $markerRepository = NULL;
	
	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		// t3jquery
		$t3jqueryCheck = T3jquery::check();
		
		// Add jQuery?
		if ($t3jqueryCheck === false) {
			if ($this->settings['addJquery']) {
				if ($this->settings['addToFooter']) {
					$GLOBALS['TSFE']->additionalFooterData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.2.min.js"></script>';
				} else {
					$GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.2.min.js"></script>';
				}
			}
		}
		
		$maps = $this->mapsRepository->findAll();
		
		// Add JS files
		$GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_api'] = '<script type="text/javascript" src="' . $this->settings['apiUrl'] . '"></script>';
		
		if ($this->settings['addToFooter']) {
			$GLOBALS['TSFE']->additionalFooterData['medgooglemaps_js'] = '<script type="text/javascript" src="' . $this->settings['jsFile'] . '"></script>';
		} else {
			$GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_js'] = '<script type="text/javascript" src="' . $this->settings['jsFile'] . '"></script>';
		}
		
		// Controls
		$controls = $this->settings['controls'];
		
		if ($controls) {
			$controlsArr    = explode(",", $controls);
			$controlsConfig = ',';
			foreach ($controlsArr as $c) {
				if ($c == 'scaleControl') {
					$controlsConfig .= '
                        ' . $c . ': true,
                    ';
				} else {
					$controlsConfig .= '
                        ' . $c . ': false,
                    ';
				}
			}
			$controlsConfig = substr($controlsConfig, 0, -1);
		}
		
		// Marker new
		$markers = explode(",", $this->settings['marker']);
		
		$markers = $this->markerRepository->setRespectStoragePage(false)->findByFilter(array(
			array(
				array(
					'uid',
					'in',
					$markers
				)
			)
		));
		
		$cObj = $this->configurationManager->getContentObject();
		$uid  = $cObj->data['uid'];
		
		// Template vars
		$this->view->assign('settings', $this->settings);
		$this->view->assign('uid', $uid);
		
		/*
		JS
		*/
		
		// Lat + Lng
		$latitude  = $this->settings['latitude'];
		$longitude = $this->settings['longitude'];
		
		if ($latitude)
			$latitude = $latitude;
		else
			$latitude = 0;
		
		if ($longitude)
			$longitude = $longitude;
		else
			$longitude = 0;
		
		// Zoom
		$zoom = $this->settings['zoom'];
		
		// Style
		$style = $this->settings['style'];
		if ($style) {
			$theme = $this->getPartialView('Theme-' . $this->settings['style'], array(), 'Themes/');
			
			$styleOutput = 'styles: ' . $theme->render() . ',';
		}
		
		// Map type
		$mapType = $this->settings['mapType'];
		if ($mapType) {
			$mapTypeOutput = $mapType;
		} else {
			$mapTypeOutput = 'MapTypeId.ROADMAP';
		}
		
		// Markers
		if ($markers) {
			$i = 1;
			foreach ($markers as $marker) {
				// Clear vars
				$infoTextOutput   = '';
				$navigationOutput = '';
				
				if ($marker->getInfotext()) {
					$infotext = $this->pi_RTEcssText($marker->getInfotext(), $cObj);
					$infotext = str_replace(array(
						"\r",
						"\n"
					), "", $infotext);
					
					$infoTextOutput = '\'<div class="medgooglemaps_content">' . $infotext . '</div>\'';
					
					if ($marker->getNavigation()) {
						$infoTextOutput .= ',';
						
						// Config for fluid standalone view for markers
						$confArr = array(
							'settings' => array(
								'googleMapsSubmitClasses' => $this->settings['googleMapsSubmitClasses']
							),
							'marker' => $marker
						);
						
						$navigation       = $this->getPartialView('Navigation', $confArr);
						$navigationOutput = json_encode($navigation->render());
					}
				}
				
				$markerOutput .= "
                    var contentString" . $i . " = 
                    [" . $infoTextOutput . "
                    " . $navigationOutput . "
                    ].join('\\n');
                ";
				
				// Locations
				if ($marker->getInfowindowautoopen())
					$infoWindowAutoOpen = $marker->getInfowindowautoopen();
				else
					$infoWindowAutoOpen = 0;
				
				if ($this->settings['customMarkerPath'])
					$customMarkerPath = $this->settings['customMarkerPath'];
				else
					$customMarkerPath = 'uploads/pics/';
				
				if ($marker->getMarkercustom())
					$markerCustom = $customMarkerPath . $marker->getMarkercustom();
				
				$locationsOutput .= "
                        [contentString" . $i . ", " . $marker->getLatitude() . ", " . $marker->getLongitude() . ", " . $infoWindowAutoOpen . ", '" . $marker->getMarkercolor() . "', '" . $markerCustom . "']
                ";
				
				if ($i < sizeof($markers))
					$locationsOutput .= ",";
				
				$i++;
			}
			
			$locationsOutput = "
                var locations = [
                    " . $locationsOutput . "
                ];
            ";
			
			$autocalcmapcenter = $this->settings['autocalcmapcenter'];
			
			$processMarker = '
                var i;

                for (i = 0; i < locations.length; i++) {';
			
			// Wenn autocalcmapcenter gesetzt sind, Umrandung der Marker erweitern
			if ($autocalcmapcenter)
				$processMarker .= 'bounds.extend(new google.maps.LatLng(locations[i][1], locations[i][2]));';
			
			$processMarker .= 'if(locations[i][4]) {
                        var markerIcon = "' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Icons/marker2/"+locations[i][4]+".png";
                    }
                    else {
                        var markerIcon = "";
                    }

                    if(locations[i][5]) {
                        var markerIcon = locations[i][5];   
                    }

                    marker[i] = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map' . $uid . ',
                        icon: markerIcon
                    });
                    
                    if(locations[i][0]) {
                        marker[i].medInfoWindow = new google.maps.InfoWindow({
                            content: locations[i][0],
                            disableAutoPan: true
                        });

                        google.maps.event.addListener(marker[i], "click", function() {
                            // Close previously opened infowindows
                            for (i = 0; i < locations.length; i++) { 
                                marker[i].medInfoWindow.close();
                            }

                            this.medInfoWindow.disableAutoPan = false;
                            this.medInfoWindow.open(map' . $uid . ',this);
                        }); 
      
                        if(locations[i][3] == 1) {
                            autoOpen.push(i);
                        }
                    }
                }
            ';
		}
		
		if (intval($this->settings['deactivateScrollWheel']) != 0)
			$scrollWheel = 'false';
		else
			$scrollWheel = 'true';
		
		$js = '<script type="text/javascript">';
		
		$js .= '
            var map' . $uid . ';
            var marker = [];
            var autoOpen = [];

            function initialize() {
                var myLatlng' . $uid . ' = new google.maps.LatLng(' . $latitude . ',' . $longitude . ');

                var myOptions' . $uid . ' = {
                	scrollwheel: ' . $scrollWheel . ',
                    zoom: ' . $zoom . ',
                    ' . $styleOutput . '
                    center: myLatlng' . $uid . ',
                    mapTypeId: google.maps.' . $mapTypeOutput . '
                    ' . $controlsConfig . '
                }
                map' . $uid . ' = new google.maps.Map(document.getElementById("map_canvas_' . $uid . '"), myOptions' . $uid . ');';
		
		if ($autocalcmapcenter)
			$js .= 'var bounds = new google.maps.LatLngBounds();';
		
		$js .= $markerOutput . '
                ' . $locationsOutput . '
                ' . $processMarker;
		
		if ($autocalcmapcenter) {
			$js .= 'map' . $uid . '.fitBounds(bounds);';
			
			$js .= 'map' . $uid . '.panToBounds(bounds);';
			
			$js .= 'var boundsChangedListener = google.maps.event.addListenerOnce(map' . $uid . ', \'bounds_changed\', function(event) {
						if(this.getZoom())
							this.setZoom(myOptions' . $uid . '.zoom);
						google.maps.event.removeListener(boundsChangedListener);
					});';
		}
		
		$js .= 'google.maps.event.addListenerOnce(map' . $uid . ', "idle", function() {
                    if(autoOpen.length > 0) {
                        for (var i = 0; i < autoOpen.length; i++) {
                            marker[autoOpen[i]].medInfoWindow.open(map' . $uid . ', marker[autoOpen[i]]);
                        }
                    }
                });
            }

            google.maps.event.addDomListener(window, "load", initialize); 
        ';
		
		$js .= '</script>';
		
		$GLOBALS['TSFE']->additionalFooterData['medgooglemaps'] .= $js;
	}
	
	protected function getPartialView($templateName, array $variables = array(), $prefix = '') {
		$partialView = new \TYPO3\CMS\Fluid\View\StandaloneView();
		$partialView->setFormat('html');
		$extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$templateRootPath              = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['partialRootPath']);
		$templatePathAndFilename       = $templateRootPath . $prefix . $templateName . '.html';
		$partialView->setTemplatePathAndFilename($templatePathAndFilename);
		$partialView->assignMultiple($variables);
		$extensionName = $this->request->getControllerExtensionName();
		$partialView->getRequest()->setControllerExtensionName($extensionName);
		
		return $partialView;
	}
	
	/**
	 * Will process the input string with the parseFunc function from tslib_cObj based on configuration set in "lib.parseFunc_RTE" in the current TypoScript template.
	 * This is useful for rendering of content in RTE fields where the transformation mode is set to "ts_css" or so.
	 * Notice that this requires the use of "css_styled_content" to work right.
	 *
	 * @param   string      The input text string to process
	 * @return  string      The processed string
	 * @see tslib_cObj::parseFunc()
	 */
	public function pi_RTEcssText($str, $cObj) {
		$parseFunc = $GLOBALS['TSFE']->tmpl->setup['lib.']['parseFunc_RTE.'];
		if (is_array($parseFunc))
			$str = $cObj->parseFunc($str, $parseFunc);
		return $str;
	}
	
	protected function debug($var) {
		print_r("<pre>") . print_r($var) . print_r("</pre>");
	}
	
}